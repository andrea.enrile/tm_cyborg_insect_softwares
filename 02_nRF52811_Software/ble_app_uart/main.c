/**
 * Copyright (c) 2014 - 2019, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 *
 * @defgroup ble_sdk_uart_over_ble_main main.c
 * @{
 * @ingroup  ble_sdk_app_nus_eval
 * @brief    UART over BLE application main file.
 *
 * This file contains the source code for a sample application that uses the Nordic UART service.
 * This application uses the @ref srvlib_conn_params module.
 */


#include <stdint.h>
#include <string.h>
#include "nordic_common.h"
#include "nrf.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "nrf_sdh.h"
#include "nrf_sdh_soc.h"
#include "nrf_sdh_ble.h"
//#include "nrf_ble_gatt.h"
//#include "nrf_ble_qwr.h"
#include "app_timer.h"
#include "app_util_platform.h"
//#include "bsp_btn_ble.h"
#include "nrf_pwr_mgmt.h"

#include "nrf_drv_saadc.h"
#include "nrf_drv_clock.h"
#include "nrf_drv_rtc.h"
#include "nrf_delay.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define APP_BLE_CONN_CFG_TAG            1                                           /**< A tag identifying the SoftDevice BLE configuration. */

#define DEVICE_NAME                     "Cyborg_Insect"                                 /**< Name of device. Will be included in the advertising data. */

#define APP_BLE_OBSERVER_PRIO           3                                           /**< Application's BLE observer priority. You shouldn't need to modify this value. */

#define APP_ADV_MAX_EVTS                (5)
#define APP_ADV_INTERVAL                16000                                       /**< The advertising interval (in units of 0.625 ms. This value corresponds to 40 ms). */
#define APP_ADV_DURATION_UNLIMITED      0                                           /**< Unlimited period .*/
#define APP_ADV_DURATION                APP_ADV_DURATION_UNLIMITED                  /**< The advertising duration (180 seconds) in units of 10 milliseconds. */

#define TX_POWER_LEVEL (0) /**< TX Power Level value. This will be set both in the TX Power service, in the advertising data, and also used to set the radio transmit power. */

#define DEAD_BEEF                       0xDEADBEEF                                  /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

#define ADV_ADDL_MANUF_DATA_LEN         (24)

#define SAADC_CHANNEL (0)

/**< Value of the additional manufacturer specific data that will be placed in air (initialized to all zeros). */
static uint8_t m_addl_adv_manuf_data[ADV_ADDL_MANUF_DATA_LEN]  = "CDEFGHIJKLMNOPQRSTUVWXYZ";

static bool m_advertising_is_running = false;


static uint8_t m_adv_handle = BLE_GAP_ADV_SET_HANDLE_NOT_SET;           /**< Advertising handle used to identify an advertising set. */
static uint8_t m_enc_advdata[BLE_GAP_ADV_SET_DATA_SIZE_MAX];            /**< Buffer for storing an encoded advertising set. */
static uint8_t m_enc_srp_data[BLE_GAP_ADV_SET_DATA_SIZE_MAX];           /**< Buffer for storing an encoded scan data. */

/**@brief Struct that contains pointers to the encoded advertising data. */
static ble_gap_adv_data_t m_adv_data =
{
        .adv_data =
        {
                .p_data = m_enc_advdata,
                .len = BLE_GAP_ADV_SET_DATA_SIZE_MAX
        },
        .scan_rsp_data =
        {
                .p_data = m_enc_srp_data,
                .len = BLE_GAP_ADV_SET_DATA_SIZE_MAX

        }
};

static void advertising_start(void);
static void advertising_data_update(uint8_t* data);

/**@brief Function for assert macro callback.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyse
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num    Line number of the failing ASSERT call.
 * @param[in] p_file_name File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
        app_error_handler(DEAD_BEEF, line_num, p_file_name);
}

/**@brief Function for initializing the timer module.
 */
static void timers_init(void)
{
        ret_code_t err_code = app_timer_init();
        APP_ERROR_CHECK(err_code);
}


///**@brief Function for handling advertising events.
// *
// * @details This function will be called for advertising events which are passed to the application.
// *
// * @param[in] ble_adv_evt  Advertising event.
// */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
          printf("Entered on_adv_evt\n");
}


/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
        uint32_t err_code;
        nrf_saadc_value_t sample;
        uint8_t samples[ADV_ADDL_MANUF_DATA_LEN] = {};
        int i = 0;

        printf("Entered ble_evt_handler\n");

        if(p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_SET_TERMINATED){
                printf("Terminated\n");
                m_advertising_is_running = false;
                for(i; i<ADV_ADDL_MANUF_DATA_LEN; i++)
                {
                        err_code = nrfx_saadc_sample_convert(SAADC_CHANNEL, &sample);
                        APP_ERROR_CHECK(err_code);

                        printf("Sample: %i\n", sample);

                        samples[i] = sample;

                        nrf_delay_ms(50);
                }

                advertising_data_update(samples);
        }
        else{
              printf("Received other event than BLE_GAP_EVT_ADV_SET_TERMINATED -> %d\n", p_ble_evt->header.evt_id);
        }
}


/**@brief Function for the SoftDevice initialization.
 *
 * @details This function initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
        ret_code_t err_code;

        err_code = nrf_sdh_enable_request();
        APP_ERROR_CHECK(err_code);

        // Configure the BLE stack using the default settings.
        // Fetch the start address of the application RAM.
        uint32_t ram_start = 0;
        err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
        APP_ERROR_CHECK(err_code);

        // Enable BLE stack.
        err_code = nrf_sdh_ble_enable(&ram_start);
        APP_ERROR_CHECK(err_code);

        err_code = sd_power_mode_set(NRF_POWER_MODE_LOWPWR);
        APP_ERROR_CHECK(err_code);

        err_code = sd_power_dcdc_mode_set(NRF_POWER_DCDC_ENABLE);
        APP_ERROR_CHECK(err_code);

        // Register a handler for BLE events.
        NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}


/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void){
        ret_code_t err_code;
        ble_advdata_t advdata;
        ble_advdata_t srdata;
        ble_advdata_manuf_data_t manuf_data;

        memset(&advdata, 0, sizeof(advdata));

        advdata.include_appearance = false;
        advdata.flags              = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

        // Build and set advertising data.
        memset(&advdata, 0, sizeof(advdata));

        manuf_data.company_identifier = 0x4142;
        manuf_data.data.size          = ADV_ADDL_MANUF_DATA_LEN;
        manuf_data.data.p_data        = m_addl_adv_manuf_data;
        advdata.p_manuf_specific_data = &manuf_data;

        advdata.name_type = BLE_ADVDATA_NO_NAME;
        advdata.include_appearance = false;
        advdata.flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

        memset(&srdata, 0, sizeof(srdata));

        ble_gap_adv_params_t adv_params;

        // Set advertising parameters.
        memset(&adv_params, 0, sizeof(adv_params));

        err_code = ble_advdata_encode(&advdata, m_adv_data.adv_data.p_data, &m_adv_data.adv_data.len);
        APP_ERROR_CHECK(err_code);

        err_code = ble_advdata_encode(&srdata, m_adv_data.scan_rsp_data.p_data, &m_adv_data.scan_rsp_data.len);
        APP_ERROR_CHECK(err_code);

        adv_params.primary_phy = BLE_GAP_PHY_CODED;
        adv_params.secondary_phy = BLE_GAP_PHY_CODED;
        adv_params.properties.type = BLE_GAP_ADV_TYPE_EXTENDED_NONCONNECTABLE_NONSCANNABLE_UNDIRECTED; //BLE_GAP_ADV_TYPE_EXTENDED_CONNECTABLE_NONSCANNABLE_UNDIRECTED;

        adv_params.duration = APP_ADV_DURATION;
        adv_params.p_peer_addr = NULL;
        adv_params.filter_policy = BLE_GAP_ADV_FP_ANY;
        adv_params.interval = APP_ADV_INTERVAL;
        adv_params.max_adv_evts = APP_ADV_MAX_EVTS;

        err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_adv_data, &adv_params);
        APP_ERROR_CHECK(err_code);

        err_code = sd_ble_gap_tx_power_set(BLE_GAP_TX_POWER_ROLE_ADV, m_adv_handle, TX_POWER_LEVEL);
        APP_ERROR_CHECK(err_code);
}


/**@brief Function for initializing power management.
 */
static void power_management_init(void)
{
        ret_code_t err_code;
        err_code = nrf_pwr_mgmt_init();
        APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling the idle state (main loop).
 *
 * @details If there is no pending log operation, then sleep until next the next event occurs.
 */
static void idle_state_handle(void)
{
        if (NRF_LOG_PROCESS() == false)
        {
                printf("NRF52811 Peripheral Sleep\n");
                nrf_pwr_mgmt_run();
        }
}

/**@brief Function to update the advertising data
 *
 *
 */
static void advertising_data_update(uint8_t* data)
{
        ret_code_t err_code;
        ble_advdata_manuf_data_t manuf_data;
        ble_advdata_t new_advdata;
        ble_gap_adv_data_t m_new_adv_data;
        memset(&m_new_adv_data, 0, sizeof(m_new_adv_data));

        memset(&new_advdata, 0, sizeof(new_advdata));

        uint8_t m_new_addl_adv_manuf_data[ADV_ADDL_MANUF_DATA_LEN]  = "";

        for (int i = 0; i < ADV_ADDL_MANUF_DATA_LEN; i++)
        {
                printf("Data %i: %i\n", i, data[i]);
                m_new_addl_adv_manuf_data[i] = data[i];
        }

        manuf_data.company_identifier = 0x4142;
        manuf_data.data.size          = ADV_ADDL_MANUF_DATA_LEN;
        manuf_data.data.p_data        = m_new_addl_adv_manuf_data;
        new_advdata.p_manuf_specific_data = &manuf_data;

        m_new_adv_data.adv_data.p_data = m_enc_advdata;
        m_new_adv_data.adv_data.len = BLE_GAP_ADV_SET_DATA_SIZE_MAX; 

        err_code = ble_advdata_encode(&new_advdata, m_new_adv_data.adv_data.p_data, &m_new_adv_data.adv_data.len);
        APP_ERROR_CHECK(err_code);

        m_new_adv_data.scan_rsp_data.p_data = NULL;
        m_new_adv_data.scan_rsp_data.len    = 0;

        ble_gap_adv_params_t adv_params;

        // Set advertising parameters.
        memset(&adv_params, 0, sizeof(adv_params));

        adv_params.primary_phy = BLE_GAP_PHY_CODED;
        adv_params.secondary_phy = BLE_GAP_PHY_CODED;
        adv_params.properties.type = BLE_GAP_ADV_TYPE_EXTENDED_NONCONNECTABLE_NONSCANNABLE_UNDIRECTED; //BLE_GAP_ADV_TYPE_EXTENDED_CONNECTABLE_NONSCANNABLE_UNDIRECTED;

        adv_params.duration = APP_ADV_DURATION;
        adv_params.p_peer_addr = NULL;
        adv_params.filter_policy = BLE_GAP_ADV_FP_ANY;
        adv_params.interval = APP_ADV_INTERVAL;
        adv_params.max_adv_evts = APP_ADV_MAX_EVTS;

        if (m_advertising_is_running != false)
        {
                err_code = sd_ble_gap_adv_stop(m_adv_handle);
                APP_ERROR_CHECK(err_code);
                m_advertising_is_running = false;
        }

        err_code = sd_ble_gap_adv_set_configure(&m_adv_handle, &m_new_adv_data, &adv_params);
        APP_ERROR_CHECK(err_code);

}

/**@brief Function for starting advertising.
 */
static void advertising_start(void)
{
       if (m_advertising_is_running != true)
       {
               ret_code_t err_code;
               err_code = sd_ble_gap_adv_start(m_adv_handle, APP_BLE_CONN_CFG_TAG);
               APP_ERROR_CHECK(err_code);
               m_advertising_is_running = true;
       }

}


void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{

}


void saadc_init(void)
{
    ret_code_t err_code;
    nrf_saadc_channel_config_t channel_config =
        NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);

    nrf_drv_saadc_config_t saadc_configs;

    saadc_configs.resolution = NRF_SAADC_RESOLUTION_8BIT;       ///< Resolution configuration.
    saadc_configs.oversample = NRF_SAADC_OVERSAMPLE_DISABLED;   ///< Oversampling configuration.
    saadc_configs.low_power_mode = true;                        ///< Indicates if low power mode is active.


    err_code = nrf_drv_saadc_init(&saadc_configs, saadc_callback);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(SAADC_CHANNEL, &channel_config);
    APP_ERROR_CHECK(err_code);
}


/**@brief Application main function.
 */
int main(void)
{

        // Initialize.
        timers_init();
        power_management_init();
        ble_stack_init();
        advertising_init();
        saadc_init();

        // Start execution.
        printf("NRF52811 Peripheral Role Start\n");

        // Enter main loop.
        for (;;)
        {
                advertising_start();
                idle_state_handle();
        }
}


/**
 * @}
 */
