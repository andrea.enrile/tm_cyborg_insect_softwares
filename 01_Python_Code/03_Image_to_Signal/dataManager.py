import numpy as np
from scipy.signal import find_peaks, medfilt

class dataManage():
    """This class allows to manage data and split them into subparts.
    #TODO: Implement an online data reading that allows to save a defined number of data, drop NaNs and let data available for processings
    Args:
        param1 (list): list of the channel recordings signals. The list has 8 very long elements

    Methods:
        npArray: return data rapresentation in a np.array (matrix) specifing the initial instat and the final one (so, the length or the array)
        """

    def __init__(self, X, filter, lb=None, ub=None):

        if lb is None:
            lb = 0
        if ub is None:
            ub = -1

        data = []
        for i in range(len(X)):
            if filter is 'not':
                data.append(X[i][lb:ub])
            if filter is 'filter':
                data.append(medfilt(X[i][lb:ub], kernel_size=3))
            if filter not in ['not', 'filter']: raise Exception('filter or not')

        self.__data = data

    def get_npArray(self):
          return np.array(self.__data)

def dataOpen(dir, count=None):
    if count is None:
        count = -1

    mV = []

    for i in range(0, 8):
        channel = np.fromfile(dir + 'channel' + str(i), dtype='int32', count=count)
        # channel = np.fromfile(dir + str(i) + '.raw', dtype='int32', count=count)
        channel = channel * 0.000298
        mV.append(channel)
    return mV
