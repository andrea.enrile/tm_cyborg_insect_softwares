import numpy as np
import scipy
from scipy.signal import find_peaks, medfilt
import matplotlib.pyplot as plt
from sklearn.decomposition import FastICA

def whitening_data(X):
    """
    Perform centering and data-whitening on a NxM matrix.
    If matrix is just a row vector the function just standardizes its values
    :param X: Data matrix
            N = number of channels/microphones/sensors
            M = number of observation along time (500*seconds of recording)
    :return: Whitened data
    """

    if X.shape[0] > 1:
        X = X - X.mean(axis=1, keepdims=True)
        cov = np.cov(X)
        d, E = np.linalg.eigh(cov)
        D = np.diag(d)
        D_inv = np.sqrt(np.linalg.inv(D))
        X_whiten = np.dot(E, np.dot(D_inv, np.dot(E.T, X)))
    else:
        X = X - X.mean()
        Var = np.std(X)
        X_whiten = X / Var
    return X_whiten

class evMetrics:
    """ This class allows to calculate the object function used to perform FastICA
    The input data must be whitened before processing (otherwise you may get meaningless results)

    Args:
        param1 (np.array): Matrixes NxM of the whitened data
        param2 (function): Optional acivation function used to calculate neg-entropy, dafault is log(cosh(x))

    Methods:
        Kurtosis: return a vector of the kurtosis performed over rows
        Neg-Entropy: return a vector of the neg-entropy (approximized) performed over rows
        Neg-Entropy-Approx: return E(x**4) where x is the values array. This should be the term to be maximized in a neg-entropy approx

    see: "Independent Component Analysis: Algorithms and Applications", Aapo Hyvärinen and Erkki Oja, Helsinki University of Technology, 2000

    """

    def __init__(self, X, g=None):
        self.__data = X
        self.__kurtosis = scipy.stats.kurtosis(self.__data, axis=1)

        def r(x):
            return np.log(np.cosh(x))

        if g is None:
            self.__function = r
        else:
            self.__function = g
        self.__neg_entropy = ((((self.__data) ** 3).mean(axis=1) ** 2) / 12 + (((self.__kurtosis) ** 2) / 48))
        self.__neg_entropy_approx = self.__function(self.__data).mean(axis=1)

    def kurtosis(self):
        return self.__kurtosis

    def neg_entropy(self):
        return self.__neg_entropy

    def neg_entropy_approx(self):
        return self.__neg_entropy_approx

def metrics_evolution(x, step):
    """ This compute the metrics evolution (using the evMetrics class) over long serie of data (long arrays)

    :param x: (np.array) Data matrix (NxM)
    :param step: time window used to estime the kurtosis/neg-entropy
    :return: (np.array) Array of values which size will be Nx(M//step)
    """

    norma_K = []
    for j in range(step, x.shape[1], step):
        signal = x[:, j - step:j]
        metrics = evMetrics((signal))
        nK = np.linalg.norm(metrics.kurtosis(), 2)
        norma_K.append(nK)
    return (np.array(norma_K))

def n_IC(X, tol=None):

    """ Compute the number of indipendent components using FastICA decomposition
    by computing the determinant of a series of Un-mixing matrixes (not really efficient)

    :param X: (np.array) Data matrix (NxM)
    :param tol: (float) a tolerance
    :return: (int) the number of indipendent component
    """
    c = []
    if tol is None:
        tol = 1e-3

    det = 1
    n_c = 1

    while det > tol:
        ica = FastICA(n_components=n_c, tol=1e-2, max_iter=2000)
        ica.fit_transform(X[0:n_c, :].T)
        M = ica.mixing_
        M /= np.linalg.norm(M, 'fro')
        det = abs(np.linalg.det(M))
        c.append(det)
        n_c += 1

    return sum(np.array(c) > tol)
    # return np.array(c), sum(np.array(c) > tol)

def observation_time(n, n_rap):
    hr = n // (500 * 60 * 60)
    min = (n - (hr * (500 * 60 * 60))) // (500 * 60)
    sec = (n - (hr * (500 * 60 * 60)) - (min * 500 * 60)) // (500)
    n_graph = (n // n_rap)
    return hr, min, sec, n_graph

def map_sources(s, intervall=None):
    if intervall is None: intervall='0,1'
    if intervall not in ['-1,1', '0,1']: raise Exception('not valid spcified range')
    maxS, minS = np.max(s), np.min(s)
    ss = []

    if intervall=='-1,1':
        for row in s:
            ss.append(((row - maxS) + (row - minS)) / (maxS - minS))
        ss=np.array(ss)
    if intervall=='0,1':
        for row in s:
            ss.append(((row - minS)) / (maxS - minS))
        ss=np.array(ss)
    return ss

