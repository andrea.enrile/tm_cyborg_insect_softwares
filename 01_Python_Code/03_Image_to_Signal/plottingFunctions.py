import random

import numpy as np
import scipy
from scipy.signal import find_peaks, medfilt
import matplotlib.pyplot as plt
from FastIcaOnData import map_sources
from sklearn.decomposition import FastICA
from dataManager import dataOpen, dataManage
from pyts.image import GramianAngularField, MarkovTransitionField
from FastIcaOnData import map_sources
from PIL import Image

class sourcesFourierTransform():
    """ The class compute the discrete Fourier transform of a vector (signal).

        :param v: (np.array) The array of the signal
        :param T: (float) Time intervall, It's the discretization limit (1/500)
        :return: x-axis and y-axis to plot the Fourier tranformation of the signal
        """

    def __init__(self, X, T=None):
        self.__original_data = X
        if T is None:
            self.__t = 1 / 500
        else:
            self.__t = T

        if len(self.__original_data.shape)>2: raise Exception('just one signal per time')

        N=len(self.__original_data)
        self.__fft = np.fft.fft(X)
        self.__y_axes = 2.0 / N * (np.abs(self.__fft[:N // 2]))
        self.__x_axes = np.linspace(0.0, 1.0 / (2.0 * self.__t), N // 2)
        self.__meaningfreq={}
        self.__noise = False

        xp, yp = find_peaks(self.__y_axes, height=0.2 * np.max(self.__y_axes))

        self.__meaningfreq[self.__x_axes[0]] = self.__y_axes[0]

        for x in xp:
            self.__meaningfreq[self.__x_axes[x]]=self.__y_axes[x]

    def getAxis(self):
        return self.__x_axes, self.__y_axes

    def getFFT(self):
        return self.__fft

    def getRelevantFrequencies(self):
        return np.array([f for f in self.__meaningfreq.keys()])

    def getRelevantFrequenciesPeaks(self):
        return np.array([p for p in self.__meaningfreq.values()])

    def __getKey(self, value, dictionary):

        for s in dictionary.keys():
            if dictionary[s] == value:
                max_peak_freq = s

        return max_peak_freq

    def __getMaxPeak(self):
        return np.max(self.getRelevantFrequenciesPeaks())

    def getHighestFreq(self):
        f=self.__getKey(self.__getMaxPeak(),self.__meaningfreq)
        return f

    def __checkNoise(self, f):
        # if all(i > 0.75*max(p) for i in p):
        #     return True
        if f > 49 and f < 51:
            return True
        if f > 99 and f < 101:
            return True
        if f > 149 and f < 151:
            return True
        if f > 199 and f < 201:
            return True
        if f > 249 and f < 251:
            return True
        if f > 15 and f < 17:
            return True

    def isNoise(self):

        # if self.__getMaxPeak()<0.12:
        #     self.__noise= True
        #     return self.__noise

        zero_peak=max([self.__meaningfreq.get(k for k in self.__meaningfreq.keys() if k<1 and k>0)])
        fifty_peak=max([self.__meaningfreq.get(k for k in self.__meaningfreq.keys() if k<52 and k>15)])

        self.__noise=self.__checkNoise(self.getHighestFreq())

        if self.__noise ==False:
            if fifty_peak/zero_peak>=0.6:
                self.__noise=True
                return self.__noise
        #
        # for k in self.__meaningfreq.keys():
        #     if k>=0 and k<1:
        #         zero_peak.append(self.__meaningfreq.get(k))
        #     if k>48 and k<52:
        #         fifty_peak.append(self.__meaningfreq.get(k))
        #
        # max_50_peak=max(fifty_peak)
        # max_0_peak=max(zero_peak)
        # ratio=max_0_peak/max_50_peak
        #
        # if ratio>0.8 and ratio<1.2:
        #     self.__noise=True
        #     return self.__noise

        return self.__noise

    def getFiltered(self, a=None, b=None):
        #TODO: Convert a & b in Hz
        if a is None: a=0
        if b is None: b=-1
        if (abs(b-a)>len(self.__original_data)): raise Exception('too many components')
        filtered_signal=np.fft.ifft(self.__fft[a:b]).real
        filtered_signal=((filtered_signal - max(filtered_signal)) + (filtered_signal - min(filtered_signal))) / (max(filtered_signal) - min(filtered_signal))
        return filtered_signal

def plot_channel(data, title):
    
    fig, axis = plt.subplots(4, 4, figsize=(20, 20))
    for xx in range(2):
        for jj in range(4):
            axis[xx, jj].plot(data[jj + 4*xx, :])
            #ASK RANGE OF MV
            axis[xx, jj].set_ylim([-300,300])
            axis[xx, jj].set_title('channel ' + str(jj + 4*xx) + '_' + title)

    for xx in range(2):
        for jj in range(4):
            x, y=sourcesFourierTransform(data[jj + 4*xx, :]).getAxis()
            axis[xx+2, jj].plot(x,y)
            #ASK RANGE OF MV
            axis[xx+2, jj].set_ylim([0,500])
            axis[xx+2, jj].set_title('fourier_channel ' + str(jj + 4*xx) + ' fft_' + title)

    return fig

def plot_channel_fourier(data, title):
    fig, axis = plt.subplots(2, 4, figsize=(20, 10), sharey=True)
    for xx in range(2):
        for jj in range(4):
            x, y=sourcesFourierTransform(data[jj + 4*xx, :]).getAxis()
            axis[xx, jj].plot(x,y)
            #ASK RANGE OF MV
            axis[xx, jj].set_ylim([-300,300])
            axis[xx, jj].set_title('fourier_channel ' + str(jj + 4*xx) + ' fft_' + title)
    return fig

def plot_sources(components, title):
    n_c = components.shape[0]

    if n_c==1: raise Exception('Just one component found')

    fig, ax = plt.subplots(2, n_c, figsize=(5 * n_c, 10))
    for h in range(n_c):
        row=map_sources(components[h,:])
        ax[0,h].plot(row, 'b')
        ax[0,h].set_ylim([-1.1, 1.1])
        ax[0,h].set_title('source' + str(h) + ' ' + title)

        x,y=sourcesFourierTransform(row).getAxis()
        ax[1,h].plot(x,y)
        ax[1,h].set_ylim([0, 1])
        ax[1,h].set_title('fourier_source' + str(h) + '_' + title)

    return fig

def create_specGrams(data, Fs):
    fig, (ax1) = plt.subplots(nrows=1)
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1)
    Pxx, _, _, im = plt.specgram(data, NFFT=2, Fs=Fs, noverlap=1, scale='dB', cmap='binary')
    ax1.axis('off')

    return fig


def create_matrix(s, image_type=None):

    s=map_sources(s)

    if image_type is None: image_type='all'
    if image_type not in ['outer', 'GADF', 'GASF', 'MTF', 'all']: raise Exception('image type not valid')

    markTF = MarkovTransitionField()
    gadf = GramianAngularField(method='difference')
    gasf = GramianAngularField(method='summation')

    X_gasf = (gasf.fit_transform(np.reshape(s, (1, -1)))+1)/2

    X_gadf = (gadf.fit_transform(np.reshape(s, (1, -1)))+1)/2
    X_mtf = (markTF.fit_transform(np.reshape(s, (1, -1)))+1)/2

    if image_type=='outer':
         m=((np.outer(s,s)))

    if image_type=='GADF':
        m = ((X_gadf[0]))

    if image_type=='GASF':
        m = ((X_gasf[0]))

    if image_type=='MTF':
        m = ((X_mtf[0]))

    if image_type=='all':
        m=np.zeros((len(s), len(s), 3))
        m[:,:,0]=((X_gadf[0]))
        m[:,:,1]= ((X_gasf[0]))
        m[:,:,2]=((X_mtf[0]))

    return (m*255).astype(np.uint8)

def plot_fourier(data, T, title):

    n_c = data.shape[0]
    noisy_components = []
    y=[]

    fig, ax = plt.subplots(1, n_c, figsize=(5 * n_c, 5), sharey=True)

    for z in range(n_c):
        fft = sourcesFourierTransform(data[z,:], T)
        x0, y0 = fft.getAxis()
        y.append(fft.getFiltered(a=1, b=100))

        if fft.isNoise():
            ax[z].plot(x0, y0, 'r')
            noisy_components.append(z)

        else:
            ax[z].plot(x0, y0, 'b')

        ax[z].set_ylim([0, 1])
        ax[z].set_title('fourier ' + str(z) + '_' + title)

    return fig, np.array(noisy_components), np.array(y)


def n_IC_exp(X, tol=None):
    """ Compute the number of indipendent components using FastICA decomposition
    by computing the determinant of a series of Un-mixing matrixes (not really efficient)

    :param X: (np.array) Data matrix (NxM)
    :param tol: (float) a tolerance
    :return: (int) the number of indipendent component
    """
    c = []
    if tol is None:
        tol = 1e-3

    det=1
    n_c=1

    while det>tol:
        ica = FastICA(n_components=n_c, tol=1e-2, max_iter=2000)
        ica.fit_transform(X[0:n_c, :].T)
        M = ica.mixing_
        M /= np.linalg.norm(M, 'fro')
        det=abs(np.linalg.det(M))
        c.append(det)
        n_c+=1

    return sum(np.array(c) > tol)


