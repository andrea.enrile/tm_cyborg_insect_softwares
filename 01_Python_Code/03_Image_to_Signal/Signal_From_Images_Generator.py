# Script to convert an image of a signal into a (Voltage, Time) coordinate to 
# be used in a signal generator
# File needed: - Image of the signals to be converted
#
#
# Made by Andrea Enrile
# 17-01-2023
#
# Install packages:
#       pip install PIL
#       pip install mathplotlib
#       pip install numpy

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import random
import shutil
import os

from FastIcaOnData import map_sources
from plottingFunctions import create_specGrams, sourcesFourierTransform, create_matrix

def my_plotter(ax, data_array, fml_data=""):
    ax.grid(True, which='both')
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
    
    ax.plot(data_array[:,1], data_array[:,0], fml_data)

def signal_resapling(data_array, frequency):
    start_time = np.min(data_array[:,1])
    end_time = np.max(data_array[:,1])

    total_time = end_time - start_time

    nbr_used_points = int(total_time*frequency)
    total_nbr_point = len(data_array[:,0])
    spacing = int(total_nbr_point/nbr_used_points)

    # Create array for (Voltage, time) pairs
    resampled_array = np.empty(shape = [1, 2])
    # Delete the row that is created by the instantiation of the numpy array     
    resampled_array = np.delete(resampled_array, 0, 0)

    for point in range(0, total_nbr_point):
        if point%spacing == 0:
            resampled_array = np.vstack([resampled_array, data_array[point,:]])
            
    return resampled_array

def real_signal_from_norm(norm_array, timeline, amplitude):
    ## Multiply time_voltage values to conver them into plausible values    
    real_value_array = norm_array.copy()
    real_value_array[:,0] = norm_array[:,0]*amplitude
    real_value_array[:,1] = norm_array[:,1]*timeline
    
    return real_value_array
    
    

def image_to_signal(image_path, save_show="save", out_file_path="", file_name="", Fs=5):
    ## Full timeline = ~8-11s
    ## Amplitude = ~350-450mV
    # Generate a random number inside the above specified values
    timeline = random.uniform(2, 10)
    amplitude = random.uniform(100, 600)
    
    # Read the image and convert it into an array for further manipulations
    im_1 = Image.open(image_path)
    ar = np.array(im_1)
    ar = ar[:,:,-2]
    
    # Find the coords of the signal in the array (signal color code is ]1, 100[)
    signal_coords = np.argwhere((ar > 1) & (ar < 100))
    # Find the coords of the axis in the array (axis color code is ]100, 255[)
    axis_coords = np.argwhere((ar > 100) & (ar < 255))

    # Create an array that is the size of the original image to add axis and signal
    # that is initialized fully to "255" (white color when converted into an image)
    image_size = im_1.size
    axis_image = np.ones(shape=(image_size[1], image_size[0]), dtype="int64")*255

    # Reconvert the coords of the axis into an image
    for rows in axis_coords:       
            axis_image[rows[0], rows[1]] = 10

    # Find the x axis by finding the row with min sum
    x_axis_pos = np.argmin([np.sum(x, axis = 0) for x in axis_image])
    # Find the y axis by finding the col (transposed to row) with min sum
    y_axis_pos = np.argmin([np.sum(x, axis = 0) for x in axis_image.T])
    
    ## Delete all rows that share the same x coord except one
    signal_coords_no_dupe = np.empty(shape = [1, 2], dtype = "int64")
    # Delete the row that is created by the instantiation of the numpy array     
    signal_coords_no_dupe = np.delete(signal_coords_no_dupe, 0, 0)

    for rows in signal_coords:
        if rows.T[1] not in signal_coords_no_dupe[:,1]:
            signal_coords_no_dupe = np.vstack([signal_coords_no_dupe, rows])
            
    ## x_axis_pos value is our y=0 value
    ## y_axis_pos value is our x=0 value
    ## Parse signal coordinate array to convert it into (Voltage, time) pairs
    ## row[0] -> y axis,  row[1] -> x axis
    # Create array for normalized (Voltage, time) pairs
    norm_time_voltage_array = np.empty(shape = [1, 2])
    # Delete the row that is created by the instantiation of the numpy array     
    norm_time_voltage_array = np.delete(norm_time_voltage_array, 0, 0)
    
    for rows in signal_coords_no_dupe:
        norm_time_voltage = np.zeros(shape = [1,2])
        # Get relative position from axes and normalise values
        norm_time_voltage[0,0] = (x_axis_pos/image_size[1] - rows[0]/image_size[1])
        norm_time_voltage[0,1] = (rows[1]/image_size[0] - y_axis_pos/image_size[0])
        
        norm_time_voltage_array = np.vstack([norm_time_voltage_array, norm_time_voltage])

    ### Plot signal only if show is selected
    if save_show == "show":   
        time_voltage_array = real_signal_from_norm(norm_time_voltage_array, timeline, amplitude)
        # Sort array from lowest x to greater for correct plotting 
        time_voltage_array_for_plot = time_voltage_array[time_voltage_array[:,1].argsort()]
    
        fig, axs = plt.subplots(2)
        
        my_plotter(axs[0], time_voltage_array_for_plot)
    
    # Generate noise to be added to the signal to modify it
    signal_amplitude = np.max(norm_time_voltage_array[:,0]) - np.min(norm_time_voltage_array[:,0])
    noise = np.random.default_rng().normal(loc=0.0, scale=signal_amplitude/30, size=len(norm_time_voltage_array))
    # Add noise to signal
    norm_time_voltage_array_noise = norm_time_voltage_array.copy()
    norm_time_voltage_array_noise[:,0] = norm_time_voltage_array[:,0] + noise

    time_voltage_array_noise = real_signal_from_norm(norm_time_voltage_array_noise, timeline, amplitude)
    # Sort array from lowest x to greater
    time_voltage_array_noise = time_voltage_array_noise[time_voltage_array_noise[:,1].argsort()]
    
    ## Resample signal at specified frequency
    resampled_signal_array = signal_resapling(time_voltage_array_noise, Fs)
    ### Plot signal only if show is selected
    if save_show == "show":   
        

        my_plotter(axs[1], time_voltage_array_noise)
        my_plotter(axs[1], resampled_signal_array, "r")
        my_plotter(axs[1], resampled_signal_array, "ro")
   
    if save_show == "save":
        if out_file_path == "": raise Exception("Output file name for saving not specified")
        with open(out_file_path+"signal/"+file_name+".csv", "w") as fp:
            np.savetxt(fp, resampled_signal_array, "%s", ",")
            
    ## Create Signals-specgrams & images

        signal = map_sources(resampled_signal_array[:,0])
        
        ## Spectrogam creation
        create_specGrams(signal, Fs)

        if save_show == 'save':
            plt.savefig(out_file_path + "images/spec_grams/" + file_name + "_SpecGram")
            plt.close()

        if save_show == 'show':
            plt.show()
            plt.close()

        y_100 = sourcesFourierTransform(signal, T=1/Fs).getFiltered(a=0, b=20)

        m=create_matrix(y_100, 'all')
        img = Image.fromarray(m)
        if save_show == 'save':
            img.save(out_file_path + "images/channels_images/" + file_name + "_Image_Channel.png")
            img.close()
        if save_show == 'show':
            img.show()
            img.close()

    
if __name__ == '__main__':
    ## Folder with the images to be converted
    images_folder_path = "../00_Images/"
    ## Folder where the generated signals need to be saved
    signals_folder_path = "../02_Signals/"
    ## Name of the PNG images
    scent_types = ["AIR", "d-3-CAREN", "EUGENOL", "LINALOOL", "NONANOL"]
    ## Number of generated signals for each image
    nbr_of_images_for_type = 2
    
    for i in range (0, len(scent_types)):
        image_path = images_folder_path+scent_types[i]+".png"
        
        signals_folder = scent_types[i]+"/"
        # Delete directory and all the file inside
        shutil.rmtree(signals_folder_path + signals_folder, ignore_errors=True)

        # Recreate Directory
        os.makedirs(signals_folder_path + signals_folder, exist_ok=True)
        os.makedirs(signals_folder_path + signals_folder + "signal/", exist_ok=True)
        os.makedirs(signals_folder_path + signals_folder + "images/spec_grams/", exist_ok=True)
        os.makedirs(signals_folder_path + signals_folder + "images/channels_images/", exist_ok=True)
    
        for j in range(0, nbr_of_images_for_type):
            file_name = str(j)
            file_name = file_name.zfill(len(str(nbr_of_images_for_type)))
            file_name = file_name + "_" + scent_types[i]
            out_file_path = signals_folder_path + signals_folder
            image_to_signal(image_path, "save", out_file_path, file_name, Fs=10)