# TM_Cyborg_Insect_Softwares

Main repository with all the code produced during the TM Cyborg Insect.

Spider project for the image_to_signal software under:
		01_Python_Code

Segger project for the software of the nRF52811 under:
		02_nRF52811_Software > ble_app_uart > pca10056e_811DK > s140 > ses
	
	In order to compile and run the software the nRF5_SDK_15.3.0 need to be downloaded https://www.nordicsemi.com/Products/Development-software/nrf5-sdk/download
	and the folders in this git repository need to be copied in the example folder 